const assert = require('assert');
const AccountNeDBRepository = require('../../public/repositories/AccountNeDBRepository');
const Account = require('../../src/account/Account');
const db = require('../../public/db');

describe('A Account NeDB Repository specification', function(){

  const repository = AccountNeDBRepository(db);

  beforeEach(async function(){
    await db.accounts.remove({}, {multi: true});
  });

  describe('list()', function(){
    it('should return empty array', async function(){
      const res = await repository.list();
      assert.deepEqual(res, []);
    });

    it('should return all accounts created in database', async function(){
      const acc = Account("teste", 0, "1");
      const acc2 = Account("teste", 0, "2");
      await repository.create(acc);
      await repository.create(acc2);
      const res = await repository.list();
      assert.deepEqual(res, [acc, acc2]);
    });
  });

  describe('create()', function(){
    it('should return the created account', async function(){
      const acc = Account("teste", 0, "1");
      const res = await repository.create(acc);
      assert.deepEqual(res, acc);
    });

    it('should create new acc in database', async function(){
      const acc = Account("teste", 0, "1");
      await repository.create(acc);
      const res = await repository.list(acc);
      assert.deepEqual(res, [acc]);
    });

    it('should throw exception case attempt to create a new account with the same id', async function(){
      await assert.rejects(async () => {
        const acc = Account("teste", 0, "1");
        const acc2 = Account("teste", 0, "1");
        await repository.create(acc);
        await repository.create(acc2);
      }, Error);
    });
  });

  describe('delete()', function(){
    it('should remove the account with id 2', async function(){
      const acc = Account("teste", 0, "1");
      const acc2 = Account("teste", 0, "2");
      await repository.create(acc);
      await repository.create(acc2);
      await repository.delete(acc2._id);
      const res = await repository.list();
      assert.deepEqual(res, [acc]);
    })

    it('should do nothing if id not exists', async function(){
      const acc = Account("teste", 0, "1");
      await repository.create(acc);
      await repository.delete("2");
      const res = await repository.list();
      assert.deepEqual(res, [acc]);
    })
  });
});
