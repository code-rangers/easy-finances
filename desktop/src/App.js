import React from 'react';
import {
  Container,
  Grid,
  Menu,
} from 'semantic-ui-react';
import Accounts from './account/account.component';
import Categories from './category/category.component';

const App = () => {

  return (
    <div>
      <Menu fixed='top' inverted>
        <Container>
          <Menu.Item as='a' header>
            Easy Finances
          </Menu.Item>
        </Container>
      </Menu>
      <Grid columns={2} padded divided>
        <Grid.Row>
          <Grid.Column width={4}>
            <Grid divided='vertically'>
              <Grid.Row>
                <Accounts/>
              </Grid.Row>
              <Grid.Row>
                <Categories />
              </Grid.Row>
            </Grid>
          </Grid.Column>
          <Grid.Column width={8}>

          </Grid.Column>
        </Grid.Row>
      </Grid>



    </div>
  )
}

export default App;
