class CrudService {

  list() {
    return this.repository.list();
  }

  findById(id) {
    return this.repository.findById(id);
  }

  create(form) {
    try {
      this.constraints(form);
    } catch(e) {
      return Promise.reject(e);
    }

    return this.repository.create({...form, _id: this.newId()});
  }

  update(entity) {
    const {_id} = entity;
    if(!_id) {
      return Promise.reject(new Error('Entity must have a id'));
    }
    try {
      this.constraints(entity);
    } catch(e) {
      return Promise.reject(e);
    }

    return this.repository.update(entity);
  }

  delete(id) {
    return this.repository.delete(id);
  }
}

export default CrudService;
