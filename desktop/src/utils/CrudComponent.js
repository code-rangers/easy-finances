const CrudComponent = superClass => class extends superClass {

  constructor(props) {
    const {entityPlural, service, formModule} = props;
    super(props);
    this.entityPlural = entityPlural;
    this.service = service;
    this.formModule = formModule;
    this.state = {
      isFormVisible: false,
      form: this.formModule.empty(),
      accounts: [],
    };
  }

  componentDidMount() {
    this.load();
  }

  load = () => this.service.list().then(entities => {
    this.setState({[this.entityPlural]: entities})
  });

  clean = () => {
    this.setState(() => ({form: this.formModule.empty()}));
    this.toggleForm();
  }

  modify = (entity) =>
    () => {
      this.setState({form: this.formModule.toForm(entity)});
      this.toggleForm();
    };

  delete = (entity) => {
    this.service.delete(entity._id).then(() =>
      this.load()
    );
  };

  save = () => {
    const form = this.state.form;
    if(!!form._id) {
      this.service
          .update(this.formModule.toEntity(form))
          .then(this.load);
    } else {
      this.service
          .create(this.formModule.toEntity(form))
          .then(this.load);
    }
    this.clean();
  }

  toggleForm = () => {
    this.setState(prevStart => ({
      isFormVisible: !prevStart.isFormVisible
    }))
  }

  handleChange = (e) =>
    this.setState({form: {...this.state.form, [e.target.name]: e.target.value}});

  openDeleteDialog = (entity) => () => {
    this.setState({
      showDeleteDialog: true,
      deleteEntity: entity,
    });
  };

  closeDeleteDialog = () => {
    this.setState({
      showDeleteDialog: false,
      deleteEntity: undefined,
    });
  };

  confirmDeleteDialog = () => {
    this.delete(this.state.deleteEntity);
    this.closeDeleteDialog();
  }
}

export default CrudComponent;
