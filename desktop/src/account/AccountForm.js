module.exports = function AccountForm(name, initialBalance) {
  return {
    name,
    initialBalance,
  };
}

module.exports.empty = () => module.exports('', '');

module.exports.getInitialBalance = (entity) => entity.initialBalance;

module.exports.updateInitialBalance = (form, initialBalance) => ({...form, initialBalance});

module.exports.toForm = (entity) => ({...entity, initialBalance: entity.initialBalance.toString()});

module.exports.toEntity = (form) => ({...form, initialBalance: parseFloat(form.initialBalance)});
