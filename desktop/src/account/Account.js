module.exports = function Account(name, initialBalance, id) {
  return {
    name,
    initialBalance,
    _id: id,
  };
}

module.exports.sumInitialBalances = (accounts) => accounts.reduce((total, account) => total + account.initialBalance, 0).toFixed(2);
