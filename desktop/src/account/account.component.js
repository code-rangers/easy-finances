import React from 'react';
import {
  Header,
  Checkbox,
  Grid,
  Button,
  Transition,
  Form,
  List,
  Confirm,
} from 'semantic-ui-react';
import {IMaskInput} from 'react-imask';
import AccountService from './AccountService';
import Mix from '../utils/MixinBuilder';
import CrudComponent from '../utils/CrudComponent';
import Account from './Account';
const AccountForm = require('./AccountForm');

class AccountPage extends Mix(React.Component).with(CrudComponent) {

  constructor(props){
    super({...props, entityPlural: 'accounts', service: new AccountService(), formModule: AccountForm});
  }

  componentDidMount() {
    super.componentDidMount();
  }

  handleInitialBalanceChange = (value) => {
    if(AccountForm.getInitialBalance(this.state.form) !== value) {
      this.setState({form: AccountForm.updateInitialBalance(this.state.form, value)});
    }
  }

  handleAccountClick = (event, item) => {
    const currentEntityIndex = this.state.currentEntityIndex === item.index ? -1 : item.index;
    this.setState({currentEntityIndex});
  };

  render = () => {
    return (
      <Grid columns={1} padded>
        <Grid.Row>
          <Header as='h2'>Accounts</Header>
        </Grid.Row>
        <Grid.Row>
          <Grid padded columns={1}>
            <Grid.Row>
              <Grid.Column>
                <List verticalAlign='middle' relaxed selection>
                  {
                    this.state.accounts.map((account, index) =>
                      <List.Item
                        key={index}
                        index={index}
                        style={{color: 'black', padding: '0'}}
                        active={this.state.currentEntityIndex === index}
                        onClick={this.handleAccountClick}
                      >
                        <List.Content floated='right'>{account.initialBalance.toFixed(2)}</List.Content>
                        <List.Content floated='left'><Checkbox/></List.Content>
                        <List.Content>{account.name}</List.Content>
                        <Transition
                          visible={this.state.currentEntityIndex === index}
                          duration={200}
                          unmountOnHide
                        >
                          <List.Content style={{paddingTop: '0.5em'}}>
                            <Button size='mini' icon='trash' color='red' circular floated='right' onClick={this.openDeleteDialog(account)}/>
                            <Button size='mini' icon='pencil' circular floated='right' onClick={this.modify(account)}/>
                          </List.Content>
                        </Transition>
                      </List.Item>
                    )
                  }
                </List>
              </Grid.Column>
            </Grid.Row>
            <Grid.Row columns={2}>
              <Grid.Column><strong>Initial balance</strong></Grid.Column>
              <Grid.Column textAlign='right'>{Account.sumInitialBalances(this.state.accounts)}</Grid.Column>
            </Grid.Row>
          </Grid>
        </Grid.Row>
        <Transition visible={this.state.isFormVisible} duration={200} unmountOnHide>
          <Grid.Row>
            <Grid.Column>
              <Form id='form' onSubmit={this.save}>
                <Form.Group widths='equal'>
                  <Form.Input
                    fluid
                    label='Name'
                    name='name'
                    value={this.state.form.name}
                    onChange={this.handleChange}
                  />
                  <Form.Input
                    label='Initial balance'
                    fluid
                    children = {
                      <IMaskInput
                        mask={Number}
                        unmask={true}
                        scale={2}
                        thousandsSeparator={'.'}
                        radix={','}
                        mapToRadix={['.']}
                        name='initialBalance'
                        onAccept={this.handleInitialBalanceChange}
                        value={this.state.form.initialBalance}
                        required
                      />
                    }
                  />
                </Form.Group>
              </Form>
            </Grid.Column>
          </Grid.Row>
        </Transition>
        <Grid.Row columns={1}>
          <Grid.Column textAlign='right'>
            {
              this.state.isFormVisible ?
              <Button.Group>
                <Button onClick={this.clean}>Cancel</Button>
                <Button.Or />
                <Button positive type='submit' form='form'>Save</Button>
              </Button.Group> :
              <Button onClick={this.toggleForm}>
                <Button.Content hidden>New</Button.Content>
              </Button>
            }
          </Grid.Column>
        </Grid.Row>
        <Confirm open={this.state.showDeleteDialog} onCancel={this.closeDeleteDialog} onConfirm={this.confirmDeleteDialog}/>
      </Grid>
    )
  }

}

export default AccountPage
