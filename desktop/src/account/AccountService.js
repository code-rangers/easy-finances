import CrudService from '../utils/CrudService';

export default class AccountService extends CrudService {

  constructor(){
    super();
    this.repository = window.api.AccountRepository;
    this.newId = window.api.newId;
  }

  constraints({name, initialBalance}) {
    if(name.length < 1) {
      throw new Error('Name can\'t be empty');
    }
    if(typeof initialBalance !== 'number'){
      throw new Error('Initial balance must be a number');
    }
    return ;
  }

}
