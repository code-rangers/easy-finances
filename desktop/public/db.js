const electron = require('electron');
const Datastore = require('nedb-promises');
const isDev = require('electron-is-dev');

const dbProdFactory = (fileName) =>
  Datastore.create({
    filename: `${(electron.app || electron.remote.app).getPath('userData')}/data/${fileName}`,
    timestampData: true,
    autoload: true
  });

const dbDevFactory = () => Datastore.create({inMemoryOnly: true, autoload: true});

const dbFactory = (fileName) => process.env.NODE_ENV === 'dev' || isDev  ? dbDevFactory() : dbProdFactory(fileName);

const createDbCollection = (name) => { return {[name]: dbFactory(`${name}.db`)}};


const db = {
  ...createDbCollection('accounts'),
  ...createDbCollection('categories'),
};
module.exports = db
