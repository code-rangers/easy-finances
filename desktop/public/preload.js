const { contextBridge, ipcRenderer } = require('electron');
const repositoryFactory = require('./repositories/RendererRepository.factory');
const { v4 } = require('uuid');
const repositoryModule = require('./repositories/repository.module');

contextBridge.exposeInMainWorld(
  "api",
  Object.keys(repositoryModule).reduce((acc, repositoryName) => {
    return {
      ...acc,
      ...repositoryFactory(
        (event, ...args) => ipcRenderer.on(event, ...args),
        (event, ...args) => ipcRenderer.send(event, ...args),
        repositoryName,
        repositoryModule[repositoryName]
      ),
    }
  }, {
    'newId': () => v4().replace(/-/g, ''),
  })
)
