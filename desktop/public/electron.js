const { app, BrowserWindow, ipcMain } = require('electron');
const path = require('path');
const isDev = require('electron-is-dev');
const db = require('./db');
const mainRepositoryFactory = require('./repositories/MainRepository.factory');
const repositoryModule = require('./repositories/repository.module');

let mainWindow;

function createWindow() {

  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: false,
      contextIsolation: true,
      enableRemoteModule: false,
      preload: path.join(__dirname, "..", "build", "preload.js")
    }
  });

  if (isDev) {
    mainWindow.webContents.openDevTools();
  }

  mainWindow.on('closed', () => {
    mainWindow = null;
  });

  mainWindow.loadURL(
    isDev ? 'http://localhost:3000' : `file://${path.resolve(__dirname, '..', 'build', 'index.html')}`,
  );

}

Object.keys(repositoryModule).forEach(repositoryName => {
  mainRepositoryFactory(
    (event, ...args) => ipcMain.on(event, ...args),
    (event, ...args) => mainWindow.webContents.send(event, ...args),
    repositoryName,
    repositoryModule[repositoryName](db),
  );
});

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})

app.whenReady().then(createWindow)
