module.exports = function CRUDNeDBRepository(db, getColletion) {
  const collection = () => getColletion(db);
  return {
    create: (entity) => {
      return collection().insert(entity);
    },
    list: () => {
      return collection().find();
    },
    delete: (id) => {
      return collection().remove({_id: id});
    },
    update: (entity) => {
      return collection().update({_id: entity._id}, entity, {});
    },
    findById: (id) => {
      return collection().find({_id: id});
    },
  }
}
