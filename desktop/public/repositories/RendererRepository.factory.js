module.exports = function(subscribe, send, name, repositoryConstructor) {
  const methods = Object.keys(repositoryConstructor());

  const methodsObject = methods.map(methodName => {
    return {
      [methodName]: (...args) => {
        return new Promise((resolve, reject) => {
          const eventName = `@${name}/${methodName}`;
          send(eventName, ...args);
          subscribe(eventName, (event, response) => {
            if(!!response.err){
              reject(response.err);
              return ;
            }
            resolve(response.data);
          });
        });
      }
    };
  }).reduce((acc, obj) => {return {...acc, ...obj}}, {})

  const ipcRendererRepository = {
    [name]: {
      ...methodsObject
    }
  }

  return ipcRendererRepository;
}
