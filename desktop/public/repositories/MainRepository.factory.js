module.exports = function(subscribe, send, name, repository) {
  const methods = Object.keys(repository)

  methods.forEach(methodName => {
    const eventName = `@${name}/${methodName}`;
    subscribe(eventName, (event, ...args) => {
      repository[methodName](...args).then(
        resolved => {
          send(eventName, {data: resolved});
        },
        rejected => send(eventName, {err: rejected})
      )
    })
  })

}
