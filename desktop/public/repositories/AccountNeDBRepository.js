const CRUDRepository = require('./CRUDNeDBRepository');

module.exports = function AccountNeDBRepository(db){
  return CRUDRepository(db, db => db.accounts);
};
